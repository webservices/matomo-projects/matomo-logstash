FROM docker.elastic.co/logstash/logstash:6.6.1

RUN rm -f /usr/share/logstash/config/logstash.yml
RUN rm -f /usr/share/logstash/pipeline/logstash.conf
COPY conf/logstash.conf /etc/logstash/conf.d/
COPY conf/logstash.yml /usr/share/logstash/config/logstash.yml

CMD ["-f", "/etc/logstash/conf.d/logstash.conf"]